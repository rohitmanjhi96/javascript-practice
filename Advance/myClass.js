class User{
    constructor(firstname, lastname, credits){
        this.firstname = firstname
        this.lastname = lastname
        this.credits = credits
    }
    getFullName(){
        let fullname = `${this.firstname} ${this.lastname} is my full name`
        return fullname
    }
    editName(newname){
        const myname = newname.split(' ')
        this.firstname = myname[0]
        this.lastname = myname[1]
        
    }
}

class Teacher extends User {
    constructor(firstname, lastname, credits, subject) {
        super(firstname, lastname, credits);
        this.subject = subject

    }
    getFullName(){
        let fullname = `${this.firstname} ${this.lastname} is my full name i tech ${this.subject}`
        return fullname
    }
}

const john = new Teacher('John', 'anderson', 34, 'English')
console.log(john)
console.log(john.getFullName())
john.editName('Rohit manjhi')
console.log(john.getFullName())

const sammy = new User();
console.log(sammy)