localStorage.setItem('name', 'Rohit')
localStorage.setItem('gf', 'Ladoo')
console.log(localStorage.getItem('name'))

localStorage.setItem('todo', 'Record Youtube video')
console.log(localStorage.getItem('todo'))

localStorage.setItem('todo', 'Record Youtube songs')
console.log(localStorage.getItem('todo'))

localStorage.removeItem('todo')
console.log(localStorage.getItem('todo'))
 
// You have to delete all local storage
// localStorage.clear()