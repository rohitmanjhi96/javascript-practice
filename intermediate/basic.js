let sayHello = function(){
    console.log("Greeting message for user")
    console.log('Hey User')
}

sayHello()

let sayHello1 = function(name){
    console.log("Greeting message for user")
    console.log(`Hey ${name}`)
}

sayHello1('Rohit')


let fullNameMaker = function(firstname, lastname){
    console.log('Welcome to LCO');
    console.log(`Happy to have you, ${firstname} ${lastname}`)
}

fullNameMaker('John', 'Doe')

let myAdder = function(n1, n2) {
    let added = n1 + n2
    return added
}

console.log(myAdder(3,9))